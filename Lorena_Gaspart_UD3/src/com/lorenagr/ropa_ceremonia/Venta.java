package com.lorenagr.ropa_ceremonia;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "coleccion_tienda", schema = "ropa_ceremonia_ud3", catalog = "")
public class Venta {
    private int id;
    private Date fechaSalidaEnTienda;
    private Coleccion coleccion;
    private Tienda tienda;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "fecha_salida_en_tienda")
    public Date getFechaSalidaEnTienda() {
        return fechaSalidaEnTienda;
    }

    public void setFechaSalidaEnTienda(Date fechaSalidaEnTienda) {
        this.fechaSalidaEnTienda = fechaSalidaEnTienda;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Venta venta = (Venta) o;
        return id == venta.id &&
                Objects.equals(fechaSalidaEnTienda, venta.fechaSalidaEnTienda);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fechaSalidaEnTienda);
    }

    @ManyToOne
    @JoinColumn(name = "idcoleccion", referencedColumnName = "id", nullable = false)
    public Coleccion getColeccion() {
        return coleccion;
    }

    public void setColeccion(Coleccion coleccion) {
        this.coleccion = coleccion;
    }

    @ManyToOne
    @JoinColumn(name = "idtienda", referencedColumnName = "id", nullable = false)
    public Tienda getTienda() {
        return tienda;
    }

    public void setTienda(Tienda tienda) {
        this.tienda = tienda;
    }
}
