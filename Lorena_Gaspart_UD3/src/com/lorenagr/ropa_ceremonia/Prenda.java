package com.lorenagr.ropa_ceremonia;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Prenda {
    private int id;
    private String nombre;
    private String tipo;
    private String color;
    private int talla;
    private double precio;
    private Date fechaCompra;
    private Date fechaEntrega;
    private Diseniador diseniador;
    private Coleccion coleccion;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "tipo")
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Basic
    @Column(name = "color")
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Basic
    @Column(name = "talla")
    public int getTalla() {
        return talla;
    }

    public void setTalla(int talla) {
        this.talla = talla;
    }

    @Basic
    @Column(name = "precio")
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "fecha_compra")
    public Date getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(Date fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    @Basic
    @Column(name = "fecha_entrega")
    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Prenda prenda = (Prenda) o;
        return id == prenda.id &&
                talla == prenda.talla &&
                Double.compare(prenda.precio, precio) == 0 &&
                Objects.equals(nombre, prenda.nombre) &&
                Objects.equals(tipo, prenda.tipo) &&
                Objects.equals(color, prenda.color) &&
                Objects.equals(fechaCompra, prenda.fechaCompra) &&
                Objects.equals(fechaEntrega, prenda.fechaEntrega);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, tipo, color, talla, precio, fechaCompra, fechaEntrega);
    }

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", nullable = false)
    public Diseniador getDiseniador() {
        return diseniador;
    }

    public void setDiseniador(Diseniador diseniador) {
        this.diseniador = diseniador;
    }

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", nullable = false)
    public Coleccion getColeccion() {
        return coleccion;
    }

    public void setColeccion(Coleccion coleccion) {
        this.coleccion = coleccion;
    }
}
