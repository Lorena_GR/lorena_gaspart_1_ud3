package com.lorenagr.ropa_ceremonia;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Coleccion {
    private int id;
    private String nombre;
    private String marca;
    private String estacion;
    private Date fechaCreacion;
    private List<Prenda> prendas;
    private List<Venta> ventas;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "marca")
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Basic
    @Column(name = "estacion")
    public String getEstacion() {
        return estacion;
    }

    public void setEstacion(String estacion) {
        this.estacion = estacion;
    }

    @Basic
    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coleccion coleccion = (Coleccion) o;
        return id == coleccion.id &&
                Objects.equals(nombre, coleccion.nombre) &&
                Objects.equals(marca, coleccion.marca) &&
                Objects.equals(estacion, coleccion.estacion) &&
                Objects.equals(fechaCreacion, coleccion.fechaCreacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, marca, estacion, fechaCreacion);
    }

    @OneToMany(mappedBy = "coleccion")
    public List<Prenda> getPrendas() {
        return prendas;
    }

    public void setPrendas(List<Prenda> prendas) {
        this.prendas = prendas;
    }

    @OneToMany(mappedBy = "coleccion")
    public List<Venta> getVentas() {
        return ventas;
    }

    public void setVentas(List<Venta> ventas) {
        this.ventas = ventas;
    }
}
